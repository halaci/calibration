

#include <stdio.h>
#include <stdlib.h>
#include <string>
//#include <windows.h>
#include <vector>

#include "libcbdetect/boards_from_cornres.h"
#include "libcbdetect/config.h"
#include "libcbdetect/find_corners.h"
#include "libcbdetect/plot_boards.h"
#include "libcbdetect/plot_corners.h"
#include <chrono>
#include <opencv2/opencv.hpp>
#include "fileio.h"

//proba
using namespace std;
using namespace cv;

using namespace std::chrono;

bool m_fFisheye = false;
Mat m_cameraMatrix;
Mat m_distCoeffs;
int iFlags[] = { 0
,CALIB_FIX_K3 + CALIB_FIX_K2 //1
,CALIB_FIX_K3 //ez szokott jo lenni //2
,CALIB_ZERO_TANGENT_DIST //3
,CALIB_ZERO_TANGENT_DIST + CALIB_FIX_K3 //4
,CALIB_ZERO_TANGENT_DIST + CALIB_FIX_K3 + CALIB_FIX_K2 //5
,CALIB_ZERO_TANGENT_DIST + CALIB_FIX_K1  //6
,CALIB_ZERO_TANGENT_DIST + CALIB_FIX_K2//7
,CALIB_ZERO_TANGENT_DIST + CALIB_FIX_K1 //8

,CALIB_RATIONAL_MODEL + CALIB_FIX_K3 + CALIB_FIX_K4 + CALIB_FIX_K6 //9
,CALIB_RATIONAL_MODEL + CALIB_FIX_K5 //10
,CALIB_RATIONAL_MODEL + CALIB_FIX_K4 //11
,CALIB_RATIONAL_MODEL + CALIB_FIX_K6 //12
,CALIB_RATIONAL_MODEL + CALIB_FIX_K5 + CALIB_FIX_K6 //13
,CALIB_RATIONAL_MODEL //+ CALIB_FIX_K5 + CALIB_FIX_K6 //14
,CALIB_RATIONAL_MODEL + CALIB_FIX_K3 //15
,CALIB_RATIONAL_MODEL + CALIB_FIX_K1 //16
,CALIB_RATIONAL_MODEL + CALIB_FIX_K1 + CALIB_FIX_K3//17
,CALIB_RATIONAL_MODEL + CALIB_FIX_K1 + CALIB_FIX_K3 + CALIB_FIX_K4//18*/
};

void extractpoints(const char* str, cbdetect::CornerType corner_type
	, std::vector<std::vector<cv::Point2f>>& image_points
	, std::vector<std::vector<cv::Point3f>>& world_points
	,Mat* pInput=NULL)

{
	cbdetect::Corner corners;
	std::vector<cbdetect::Board> boards;
	cbdetect::Params params;
	params.corner_type = corner_type;
	//	params.show_debug_image = true;
	//params.detect_method = DetectMethod::TemplateMatchFast;

	cv::Mat img;
	if (pInput) img = pInput->clone();
	else img= cv::imread(str, cv::IMREAD_COLOR);

	if (img.cols == 0) return ;

	auto t1 = high_resolution_clock::now();
	cbdetect::find_corners(img, corners, params);
	auto t2 = high_resolution_clock::now();
	cbdetect::plot_corners(img, corners);
	auto t3 = high_resolution_clock::now();
	cbdetect::boards_from_corners(img, corners, boards, params);
	auto t4 = high_resolution_clock::now();
	printf("Find corners took: %.3f ms\n", duration_cast<microseconds>(t2 - t1).count() / 1000.0);
	printf("Find boards took: %.3f ms\n", duration_cast<microseconds>(t4 - t3).count() / 1000.0);
	printf("Total took: %.3f ms\n", duration_cast<microseconds>(t2 - t1).count() / 1000.0 + duration_cast<microseconds>(t4 - t3).count() / 1000.0);
	//cbdetect::plot_boards(img, corners, boards, params);
	///////////////////////////////////////////////////////////////////////////////
	std::vector<cbdetect::Board>& deltilles = boards;
	cbdetect::Corner& deltille_corners = corners;
	//res[i] = std::min(params.num_boards, (int)deltilles.size());
	int iPSize = 24;
	double dx = iPSize;
	double dy = iPSize / 2.0 * std::sqrt(3);


	if (deltilles.size() == 0) return;
	for (int j = 0; j < 1/*(int)deltilles.size()*/; ++j) {
		cbdetect::Board& deltille = deltilles[j];
		std::vector<cv::Point2f> pi_buf;
		std::vector<cv::Point3f>  pw_buf;

		for (int jj = 0; jj < deltille.idx.size(); ++jj) {
			double shift = -jj * dx / 2.0;
			for (int ii = 0; ii < deltille.idx[0].size(); ++ii) {
				if (deltille.idx[jj][ii] >= 0) {
					pi_buf.emplace_back(deltille_corners.p[deltille.idx[jj][ii]]);
					pw_buf.emplace_back(Point3f(shift + dx * ii, dy * jj, 0));
				}
			}
		}

		image_points.emplace_back(pi_buf);
		world_points.emplace_back(pw_buf);

	}

}

void detect(const char* str, cbdetect::CornerType corner_type) {
	cbdetect::Corner corners;
	std::vector<cbdetect::Board> boards;
	cbdetect::Params params;
	params.corner_type = corner_type;
//	params.show_debug_image = true;
	//params.detect_method = DetectMethod::TemplateMatchFast;

	cv::Mat img = cv::imread(str, cv::IMREAD_COLOR);

	auto t1 = high_resolution_clock::now();
	cbdetect::find_corners(img, corners, params);
	auto t2 = high_resolution_clock::now();
	cbdetect::plot_corners(img, corners);
	auto t3 = high_resolution_clock::now();
	cbdetect::boards_from_corners(img, corners, boards, params);
	auto t4 = high_resolution_clock::now();
	printf("Find corners took: %.3f ms\n", duration_cast<microseconds>(t2 - t1).count() / 1000.0);
	printf("Find boards took: %.3f ms\n", duration_cast<microseconds>(t4 - t3).count() / 1000.0);
	printf("Total took: %.3f ms\n", duration_cast<microseconds>(t2 - t1).count() / 1000.0 + duration_cast<microseconds>(t4 - t3).count() / 1000.0);
	cbdetect::plot_boards(img, corners, boards, params);
	///////////////////////////////////////////////////////////////////////////////
	std::vector<cbdetect::Board>& deltilles=boards;
	cbdetect::Corner& deltille_corners=corners;
	std::vector<std::vector<cv::Point2f>> image_points;
	std::vector<std::vector<cv::Point3f>> world_points;
	//res[i] = std::min(params.num_boards, (int)deltilles.size());
	int iPSize = 24;
	double dx = iPSize;
	double dy = iPSize / 2.0 * std::sqrt(3);
	for (int j = 0; j < 1/*(int)deltilles.size()*/; ++j) {
		cbdetect::Board& deltille = deltilles[j];
		std::vector<cv::Point2f> pi_buf;
		std::vector<cv::Point3f>  pw_buf;

		for (int jj = 0; jj < deltille.idx.size(); ++jj) {
			double shift = -jj * dx / 2.0;
			for (int ii = 0; ii < deltille.idx[0].size(); ++ii) {
				if (deltille.idx[jj][ii] >= 0) {
					pi_buf.emplace_back(deltille_corners.p[deltille.idx[jj][ii]]);
					pw_buf.emplace_back(Point3f(shift + dx * ii, dy * jj,0));
				}
			}
		}
		image_points.emplace_back(pi_buf);
		world_points.emplace_back(pw_buf);
	}
	cv::Mat cameraMatrix = Mat::eye(3, 3, CV_64F);;
	vector<Mat> rvecs;
	vector<Mat> tvecs;
	Mat distCoeffs = Mat::zeros(5, 1, CV_64F);
	cv::calibrateCamera(world_points, image_points
		, img.size()
		, cameraMatrix
		, distCoeffs
		, rvecs
		, tvecs
		, 0);

	imshow("input", img);
	Mat Output;
	Mat newcameraMatrix;
	Mat map1, map2;
	//initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(), newcameraMatrix, img.size(), CV_16SC2, map1, map2);

	Size imgSize = img.size();
	newcameraMatrix = getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imgSize, 1);

	initUndistortRectifyMap(cameraMatrix, distCoeffs
		, Mat()
		//,m_MCamera
		, newcameraMatrix
		, imgSize
		//,CV_32FC1
		, CV_16SC2
		, map1, map2);


	//undistort(img, Output, cameraMatrix, distCoeffs);
	remap(img, Output, map1, map2, INTER_LINEAR);
	namedWindow("input", WINDOW_FREERATIO | WINDOW_NORMAL);
	namedWindow("output", WINDOW_FREERATIO | WINDOW_NORMAL);
	imshow("input", img);
	imshow("output", Output);

	cv::waitKey();
}

void calibration(const char* str
, std::vector<std::vector<cv::Point2f>>& image_points
,std::vector<std::vector<cv::Point3f>>& world_points,int iFlags,string sPrefix
,Mat* pResult=NULL
,Mat* pInput=NULL)
{
	cv::Mat img;

	if (pInput) img = pInput->clone();
	else img= cv::imread(str, cv::IMREAD_COLOR);

	cv::Mat cameraMatrix = Mat::eye(3, 3, CV_64F);;
	vector<Mat> rvecs;
	vector<Mat> tvecs;
	Mat distCoeffs;// = Mat::zeros(8, 1, CV_64F);//4,5,8,12,14

	if (m_fFisheye)
	{
		distCoeffs = Mat::zeros(4, 1, CV_64F);
		fisheye::calibrate(world_points, image_points
			, img.size()
			, cameraMatrix
			, distCoeffs
			, rvecs
			, tvecs
			, iFlags);

	}
	else
	{
		calibrateCamera(world_points, image_points
			, img.size()
			, cameraMatrix
			, distCoeffs
			, rvecs
			, tvecs
			, iFlags);
			
		cout << "-----------" << sPrefix << "-------------" << "\r\n";
		cout << "cameraMatrix: " << cameraMatrix << "\r\n";
		cout << "distCoeffs: " << distCoeffs << "\r\n";
		/*cout << "--------------------------------------------\r\n";
		cameraMatrix.at<double>(0, 0) = 1333.319958369202;
		cameraMatrix.at<double>(1, 1) = 1331.327809407608;
		cameraMatrix.at<double>(0, 2) = 1280.279602400912;
		cameraMatrix.at<double>(1, 2) = 921.0816533211494;

		
		///
		distCoeffs = Mat::zeros(5, 1, CV_64F);//4,5,8,12,14
		distCoeffs.at<double>(0, 0) = -0.3397606561713924;
		distCoeffs.at<double>(0, 1) = 0.1420836692813779;
		distCoeffs.at<double>(0, 2) = -0.0002080012215233211;
		distCoeffs.at<double>(0, 3) = 5.286650416900398e-05;
		distCoeffs.at<double>(0, 4) = -0.03060319678919659;
		cout << "cameraMatrix: " << cameraMatrix << "\r\n";
		cout << "distCoeffs: " << distCoeffs << "\r\n";
		cout << "--------------------------------------------\r\n";*/
/*		Mat stdE;
		Mat stdI;
		Mat stdOP,RMS;
		for (int t = 0; t < world_points.size(); t++)
		{
			world_points[t].resize(100);
			image_points[t].resize(100);
		}
		calibrateCameraRO(world_points, image_points
			, img.size()
			,50
			, cameraMatrix
			, distCoeffs
			, rvecs
			, tvecs
			, noArray()
			,stdE
			,stdI
			,stdOP
			,RMS
			, iFlags);*/

	}

	//imshow("input", img);
	Mat Output;
	Mat newcameraMatrix;
	Mat map1, map2;
	//initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(), newcameraMatrix, img.size(), CV_16SC2, map1, map2);


	m_cameraMatrix = cameraMatrix.clone();
	m_distCoeffs = distCoeffs.clone();

	Size imgSize = img.size();
	newcameraMatrix = getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imgSize, 1);
	cout << "New cameramatrix: " << newcameraMatrix << "\r\n";

	if (m_fFisheye)
	{
		fisheye::initUndistortRectifyMap(cameraMatrix, distCoeffs
			, Mat()
			//,m_MCamera
			, newcameraMatrix
			, imgSize
			//,CV_32FC1
			, CV_16SC2
			, map1, map2);

	}
	else
	{
		initUndistortRectifyMap(cameraMatrix, distCoeffs
			, Mat()
			//,m_MCamera
			, newcameraMatrix
			, imgSize
			//,CV_32FC1
			, CV_16SC2
			, map1, map2);

	}


	//undistort(img, Output, cameraMatrix, distCoeffs);
	remap(img, Output, map1, map2, INTER_LINEAR);
	//namedWindow("input", WINDOW_FREERATIO | WINDOW_NORMAL);
	namedWindow("output_"+ sPrefix, WINDOW_FREERATIO | WINDOW_NORMAL);
	//imshow("input", img);
	imshow("output_"+ sPrefix, Output);
	if (pResult) *pResult=Output.clone();
		
	//imwrite("calib_" + sPrefix + ".jpg", Output);
	//cv::waitKey();
}

void calcundistort(Mat& Input, Mat& Output,float iAlpha)
{
	Mat newcameraMatrix;
	Mat map1, map2;


	Size imgSize = Input.size();
	newcameraMatrix = getOptimalNewCameraMatrix(m_cameraMatrix, m_distCoeffs, imgSize, iAlpha);
	//cout << "New cameramatrix: " << newcameraMatrix << "\r\n";

		initUndistortRectifyMap(m_cameraMatrix, m_distCoeffs
			, Mat()
			//,m_MCamera
			, newcameraMatrix
			, imgSize
			//,CV_32FC1
			, CV_16SC2
			, map1, map2);

	


	//undistort(img, Output, cameraMatrix, distCoeffs);
	remap(Input, Output, map1, map2, INTER_LINEAR);
	//namedWindow("input", WINDOW_FREERATIO | WINDOW_NORMAL);
//	namedWindow("output_" + sPrefix, WINDOW_FREERATIO | WINDOW_NORMAL);
}

void testexisting(const char* str
		, Mat* pResult = NULL)
{
	cv::Mat img = cv::imread(str, cv::IMREAD_COLOR);

	cv::Mat cameraMatrix = Mat::eye(3, 3, CV_64F);;
	Mat distCoeffs = Mat::zeros(8, 1, CV_64F);//4,5,8,12,14



	cameraMatrix.at<double>(0, 0) = 1333.319958369202;
	cameraMatrix.at<double>(1, 1) = 1331.327809407608;
	cameraMatrix.at<double>(0, 2) = 1280.279602400912;
	cameraMatrix.at<double>(1, 2) = 921.0816533211494;


	///
	distCoeffs = Mat::zeros(5, 1, CV_64F);//4,5,8,12,14
	distCoeffs.at<double>(0, 0) = -0.3397606561713924;
	distCoeffs.at<double>(0, 1) = 0.1420836692813779;
	distCoeffs.at<double>(0, 2) = -0.0002080012215233211;
	distCoeffs.at<double>(0, 3) = 5.286650416900398e-05;
	distCoeffs.at<double>(0, 4) = -0.03060319678919659;
	
		

	//imshow("input", img);
	Mat Output;
	Mat newcameraMatrix;
	Mat map1, map2;
	//initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(), newcameraMatrix, img.size(), CV_16SC2, map1, map2);

	Size imgSize = img.size();
	newcameraMatrix = getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imgSize, 1);
	cout << "New cameramatrix: " << newcameraMatrix << "\r\n";

	
		initUndistortRectifyMap(cameraMatrix, distCoeffs
			, Mat()
			//,m_MCamera
			, newcameraMatrix
			, imgSize
			//,CV_32FC1
			, CV_16SC2
			, map1, map2);

	
	//undistort(img, Output, cameraMatrix, distCoeffs);
	remap(img, Output, map1, map2, INTER_LINEAR);
	//namedWindow("input", WINDOW_FREERATIO | WINDOW_NORMAL);
	namedWindow("output_test" , WINDOW_FREERATIO | WINDOW_NORMAL);
	//imshow("input", img);
	imshow("output_test" , Output);
	if (pResult) *pResult = Output.clone();

	//imwrite("calib_" + sPrefix + ".jpg", Output);
	//cv::waitKey();
}

Point2f rotate2d(const cv::Point2f& inPoint, const double& angRad)
{
	cv::Point2f outPoint;
	//CW rotation
	outPoint.x = std::cos(angRad)*inPoint.x - std::sin(angRad)*inPoint.y;
	outPoint.y = std::sin(angRad)*inPoint.x + std::cos(angRad)*inPoint.y;
	return outPoint;
}

Point2f rotatePoint(const cv::Point2f& inPoint, const cv::Point2f& center, const double& angRad)
{
	return rotate2d(inPoint - center, angRad) + center;
}
bool _TestAll();
int main(int argc, char** argv)
{
	/*_TestAll();
	return true;*/
	std::vector<std::vector<cv::Point2f>> image_points;
	std::vector<std::vector<cv::Point3f>> world_points;

	string sPath = "../cameras/";

	string sSubFolder = "podoscope_04_1";
	std::vector<std::string> files;
	string sExt = "png"; //"png"
	//sSubFolder = "12"; sExt = "png";
	

	get_all_file((sPath+sSubFolder).c_str(), sExt.c_str(), files);


	for (int t = 0; t < files.size(); t++)
	{
		if (files[t].find((string)"test."+sExt) != std::string::npos) continue;
		if (files[t].find((string)"h." + sExt) != std::string::npos) continue;
		extractpoints(files[t].c_str(), cbdetect::MonkeySaddlePoint, image_points, world_points);
	}

	Mat Result;
	//files[0].c_str()
	/*testexisting("../cameras/18/full/full_img1.png");
	waitKey();*/
	int iCnt = sizeof(iFlags)/sizeof(int) ;

	int iSelected =  -1;

	int iCfg;
	if (iSelected == -1)
	{
		for (int t = 0; t < iCnt; t++)
		{
			//if (iSelected!=-1) t = iSelected;
			Mat Result;
			//files[0].c_str()
			calibration((sPath + sSubFolder+"/test." + sExt).c_str(), image_points, world_points
				,iFlags[t] | CALIB_FIX_ASPECT_RATIO //| CALIB_FIX_PRINCIPAL_POINT
				, format("%d", t),&Result);
			imwrite((string)"../results/" + sSubFolder + "/" + format("cfg%d.jpg", t), Result);

			//if (iSelected != -1) break;
		}
	

		cout << "Press a key to continue.\r\n";
		waitKey();
		cout << "Enter optimal configuration ID: ";
		cin >> iCfg;
		cout << "------------- PRESS KEY ------------\r\n"; waitKey();
	}
	else iCfg = iSelected;
	
	
	destroyAllWindows();
	
	calibration((sPath + sSubFolder + "/test."+sExt).c_str(), image_points, world_points
		, iFlags[iCfg] | CALIB_FIX_ASPECT_RATIO //| CALIB_FIX_PRINCIPAL_POINT
		, format("selected result"));

	cout << "Change alpha by pressing q/a. Press x to continue.";
	float iAlpha =  0.4;
	Mat Input = imread((sPath + sSubFolder + "/test." + sExt).c_str());
	Mat Output;
	//undistort(Input, Output, m_cameraMatrix, m_distCoeffs);
	calcundistort(Input, Output, iAlpha);
	namedWindow("orig",WINDOW_NORMAL);
	imshow("orig", Output);
	if (iAlpha == 0)
	{


		while (true)
		{
			int ch = waitKey();
			//cout<<ch<<"\r\n";
			if (ch == 'q') iAlpha += 0.1;
			if (ch == 'a') iAlpha -= 0.1;
			if (ch == 'x') break;
			if (iAlpha < -1) iAlpha = -1;
			if (iAlpha > 1) iAlpha = 1;
			cout << "Alpha: " << iAlpha << "\r\n";
			calcundistort(Input, Output, iAlpha);
			imshow("output_selected result", Output);
		}
	}

	cout << "------------- PRESS KEY ------------\r\n"; waitKey();
	Mat Undist;
	///////////////////////////////////////////////
	world_points.clear();
	image_points.clear();
	for (int t = 0; t < files.size(); t++)
	{
		if (files[t].find("h." + sExt) == std::string::npos) continue;
		Mat Img = imread(files[t].c_str());
		calcundistort(Img, Undist, iAlpha);
		extractpoints("", cbdetect::MonkeySaddlePoint, image_points, world_points, &Undist);
		break;
	}
		
		//namedWindow(format("Undist%d", t).c_str(), WINDOW_NORMAL);
		//imshow(format("Undist%d", t), Undist);
		
		//
		vector<Point2f> imgPoints;
		vector<Point2f> planePoints;
		for (int k = 0; k < image_points[0].size(); k++)
		{
			imgPoints.push_back(Point2f(image_points[0][k].x, image_points[0][k].y));
			planePoints.push_back(Point2f(world_points[0][k].x, world_points[0][k].y));
			planePoints[k]=rotatePoint(planePoints[k], Point2f(0, 0), M_PI / 180 * -60);
		}
		Mat HTrO=findHomography(imgPoints, planePoints);

		Mat Homog;


		int iTrX = 0;
		int iTrY = 0;

		namedWindow("HCorrected", WINDOW_NORMAL);
		float fScale = 4;
		
		Mat H;
		while (true)
		{
			
			Mat HTr = HTr.eye(Size(3, 3), CV_64F);

			Mat HSc = HSc.eye(Size(3, 3), CV_64F);
			HSc.at<double>(0, 0) = fScale;
			HSc.at<double>(1, 1) = fScale;
			H = HSc * HTrO;

			HTr.at<double>(0, 2) = iTrX;
			HTr.at<double>(1, 2) = iTrY;
		
			H = HTr * H;

			warpPerspective(Undist, Homog, H, Input.size(), INTER_LINEAR + WARP_FILL_OUTLIERS, BORDER_CONSTANT, Scalar(0));

			imshow("HCorrected", Homog);

			int key = cv::waitKey();
			bool fShift = false;
			bool fReload = false;

			if ((key / 256) & 0x100) fShift = true;

			if (key=='q')
			{
				fScale -= 0.1;
				fReload = true;
			}
			if (key == 'w')
			{
				fScale += 0.1;
				fReload = true;
			}

			if (key == 'a')
			{
				iTrX -= 5;
				fReload = true;
			}
			if (key == 's')
			{
				iTrX += 5;
				fReload = true;
			}

			if (key == 'y')
			{
				iTrY -= 5;
				fReload = true;
			}
			if (key == 'x')
			{
				iTrY += 5;
				fReload = true;
			}

			if (key == ' ')
			{
				break;
			}
			

		}
	
	///////////////////////////////////////////////
	FileStorage fs(sPath + sSubFolder+"calibration.yml", FileStorage::WRITE);
	fs << "configuration" << iCfg;
	fs << "configurationValue" <<  (iFlags[iCfg] | CALIB_FIX_ASPECT_RATIO);
	fs << "cameraMatrix" << m_cameraMatrix << "distCoeffs" << m_distCoeffs;
	fs << "imageSize" << Input.size();
	fs << "alpha" << iAlpha;
	fs << "homography" << H;
	fs.release();
	cout << "\r\n------------- PRESS KEY ------------\r\n"; waitKey();
	///////////////////////////////////////////////
	world_points.clear();
	image_points.clear();
	
	for (int t = 0; t < files.size(); t++)
	{
		if (files[t].find("test." + sExt) != std::string::npos) continue;
		Mat Img = imread(files[t].c_str());
		
		calcundistort(Img, Undist, iAlpha);
		warpPerspective(Undist, Homog, H, Input.size(), INTER_LINEAR + WARP_FILL_OUTLIERS, BORDER_CONSTANT, Scalar(0));


		namedWindow(format("Undist%d", t).c_str(), WINDOW_NORMAL);
		imshow(format("Undist%d", t), Homog);
		extractpoints("", cbdetect::MonkeySaddlePoint, image_points, world_points,&Homog);
	}
	for (int t = 0; t < files.size(); t++)
	{
		if (files[t].find("test." + sExt) == std::string::npos) continue;
		Mat Img = imread(files[t].c_str());
		calcundistort(Img, Undist, iAlpha);
		warpPerspective(Undist, Homog, H, Input.size(), INTER_LINEAR + WARP_FILL_OUTLIERS, BORDER_CONSTANT, Scalar(0));
	}

	calibration("", image_points, world_points
		,0 //| CALIB_FIX_PRINCIPAL_POINT
		, format("final result"),NULL,&Undist);

	///////////////////////////////////////////////
	fs.open((string)"../cameras/" + sSubFolder + "calibration_dummy.yml", FileStorage::WRITE);
	fs << "configuration" << iCfg;
	fs << "configurationValue" << (iFlags[iCfg] | CALIB_FIX_ASPECT_RATIO);
	fs << "cameraMatrix" << m_cameraMatrix << "distCoeffs" << m_distCoeffs;
	fs << "imageSize" << Input.size();
	fs.release();
	///////////////////////////////////////////////
	cout << "\r\n------------- PRESS KEY ------------\r\n"; waitKey();
	return 0;

	printf("chessboards...");
	//detect("../example_data/e2.png", cbdetect::SaddlePoint);
	printf("deltilles...");
	detect("../example_data/img3.png", cbdetect::MonkeySaddlePoint);
	return 0;

	extractpoints("../example_data/img1.png", cbdetect::MonkeySaddlePoint, image_points, world_points);
	extractpoints("../example_data/img2.png", cbdetect::MonkeySaddlePoint, image_points, world_points);
	extractpoints("../example_data/img3.png", cbdetect::MonkeySaddlePoint, image_points, world_points);
	extractpoints("../example_data/img4.png", cbdetect::MonkeySaddlePoint, image_points, world_points);
	extractpoints("../example_data/img5.png", cbdetect::MonkeySaddlePoint, image_points, world_points);

	
	
	int iFisheyeFlags[] = { 0
		,fisheye::CALIB_FIX_PRINCIPAL_POINT+fisheye::CALIB_FIX_K1//1
		,fisheye::CALIB_FIX_PRINCIPAL_POINT + fisheye::CALIB_FIX_K2//ez szokott jo lenni //2
		,fisheye::CALIB_FIX_PRINCIPAL_POINT + fisheye::CALIB_FIX_K3//3
		,fisheye::CALIB_FIX_PRINCIPAL_POINT + fisheye::CALIB_FIX_K4//4
		,fisheye::CALIB_FIX_PRINCIPAL_POINT + fisheye::CALIB_FIX_K1+ fisheye::CALIB_FIX_K2//5
		,fisheye::CALIB_FIX_PRINCIPAL_POINT + fisheye::CALIB_FIX_K1+ fisheye::CALIB_FIX_K3//6
		,fisheye::CALIB_FIX_PRINCIPAL_POINT + fisheye::CALIB_FIX_K1+ fisheye::CALIB_FIX_K4//7
		,fisheye::CALIB_FIX_PRINCIPAL_POINT + fisheye::CALIB_FIX_K1+ fisheye::CALIB_FIX_K2+fisheye::CALIB_FIX_K4//8
		/*
		,CALIB_RATIONAL_MODEL + CALIB_FIX_K3 + CALIB_FIX_K4 + CALIB_FIX_K6 //9
		,CALIB_RATIONAL_MODEL + CALIB_FIX_K4 + CALIB_FIX_K6 //10
		,CALIB_RATIONAL_MODEL + CALIB_FIX_K3 + CALIB_FIX_K4 //11
		,CALIB_RATIONAL_MODEL + CALIB_FIX_K3 + CALIB_FIX_K6 //12
		,CALIB_RATIONAL_MODEL + CALIB_FIX_K3 + CALIB_FIX_K5 + CALIB_FIX_K6 //13
		,CALIB_RATIONAL_MODEL + CALIB_FIX_K5 + CALIB_FIX_K6 //14
		,CALIB_RATIONAL_MODEL + CALIB_FIX_K3 //15
		,CALIB_RATIONAL_MODEL + CALIB_FIX_K1 //16
		,CALIB_RATIONAL_MODEL + CALIB_FIX_K1 + CALIB_FIX_K3//17
		,CALIB_RATIONAL_MODEL + CALIB_FIX_K1 + CALIB_FIX_K3 + CALIB_FIX_K4//18*/
	};

	iCnt = (m_fFisheye ? sizeof(iFisheyeFlags ) / sizeof(int): sizeof(iFlags) / sizeof(int));
	for (int t = 0; t < iCnt; t++)
	{
		
		calibration("../example_data/img3.png", image_points, world_points
			, (m_fFisheye? iFisheyeFlags[t]:iFlags[t])| CALIB_FIX_ASPECT_RATIO //| CALIB_FIX_PRINCIPAL_POINT
			,format("%d",t));
		
	}
	
	waitKey();

	return 0;

	printf("chessboards...");
	//detect("../example_data/e2.png", cbdetect::SaddlePoint);
	printf("deltilles...");
	detect("../example_data/img3.png", cbdetect::MonkeySaddlePoint);

}


bool _TestAll()
{
	string sSubFolder = "12";
	std::vector<std::string> files;
	string sExt = "jpg"; //"png"
	sSubFolder = "12"; sExt = "png";
	string sPath = "../cameras/";

	get_all_file((sPath + sSubFolder).c_str(), sExt.c_str(), files);
	int iAlpha;
	Size InputSize;
	Mat HTr;

	FileStorage fs(sPath + sSubFolder + "calibration.yml", FileStorage::READ);
	
	
	fs["cameraMatrix"]>> m_cameraMatrix;
	fs["distCoeffs"]>> m_distCoeffs;
	fs["imageSize"]>> InputSize;
	fs["alpha"]>> iAlpha;
	fs["homography"] >> HTr;
	fs.release();

	return true;
}